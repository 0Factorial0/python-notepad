def clear():
    import os
    #clear screen
    test_os = os.name
    if test_os == "nt":
        os.system('cls')
    elif test_os == "posix":
        os.system('clear')
    else:
        pass

def notepad():
    
    import os
    import time

    #clear screen
    clear()

    #default variables
    isUserWriting = True
    row_count = 1
    line_list = []

    #get the current location of folder
    notepad_file_name = "notepad.py"
    file_path = os.path.dirname(os.path.abspath(notepad_file_name))
    
    #get note count
    noteCount = open(file_path+"/notes/noteCount.txt", "r")
    noteCount_ = int(noteCount.read())
    noteCount.close()

    file_name = "note"+str(noteCount_)+".txt"

    #menu
    print("--------------------------")
    print("PYX Notepad")
    print("--------------------------")
    print("Editing Note Number {0}".format(noteCount_+1))
    print("--------------------------")

    try:
        #get user input
        while isUserWriting == True:
            userinput0 = str(input("Write Line [{0}]: ".format(row_count)))
            #if //end is written it's finish line
            if userinput0 == "//end":
                isUserWriting = False
            else:
                row_count = row_count + 1
                line_list.append(userinput0)

        #write the lines
        file = open(file_path+"/notes/notes/"+file_name, "w")
        for line in line_list:
            file.write("%s\n" % line)
        
        #set note count
        noteCount0 = open(file_path+"/notes/noteCount.txt", "w")
        noteCount0.write(str(noteCount_+1))
        noteCount0.close()
        file.close()
    except:
        print("--------------------------")
        print("Unicode Error. Only Use English Characters Please.")
        time.sleep(4)

    #return notepad
    time.sleep(2)
    notepad()

notepad()
